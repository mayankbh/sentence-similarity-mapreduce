#!/bin/python2

import sys
import math

current_key = None
current_sum = 0

for line in sys.stdin:
    #Line is of the format <Sentence1_Sentence2>\tCount
    split_line = line.split('\t')
    key = split_line[0] #Sentence1_Sentence2
    value = split_line[1]   #Weight product


    if current_key != key:
        #Emit old sum
        if(current_key != None):
            split_key = current_key.split('_')
            sentence_one = split_key[0]
            sentence_two = split_key[1]
            doc1 = sentence_one.split('/')[0]
            doc2 = sentence_two.split('/')[0]
            size1 = float(sentence_one.split('/')[1])
            size2 = float(sentence_two.split('/')[1])
            similarity = current_sum / (math.sqrt(size1) * math.sqrt(size2))
            if(similarity < 0.9):
                #print '%s\t%s' % (doc1 + ":" + doc2, 'Sum = ' + str(current_sum) + ', Length product = ' + str(size1 * size2) + ', Cosine = ' + str(similarity))
                pass
            else:
#                print '%s\t%s' % (doc1 + ":" + doc2, 'Sum = ' + str(current_sum) + ', Length product = ' + str(size1 * size2) + ', Cosine = ' + str(similarity) + "\t >= 90% similar")
                print '%s\t%s' % (doc1 + ':' + doc2, similarity)
        #Reset running counts
        current_sum = 0
        current_key = key
    current_sum += int(value) #update sum


if(current_key != None):
    split_key = current_key.split('_')
    sentence_one = split_key[0]
    sentence_two = split_key[1]
    doc1 = sentence_one.split('/')[0]
    doc2 = sentence_two.split('/')[0]
    size1 = float(sentence_one.split('/')[1])
    size2 = float(sentence_two.split('/')[1])
    similarity = current_sum / (math.sqrt(size1) * math.sqrt(size2))
    if(similarity > 0.9):
        #print '%s\t%s' % (doc1 + ":" + doc2, 'Sum = ' + str(current_sum) + ', Length product = ' + str(size1 * size2) + ', Cosine = ' + str(similarity))
        print '%s\t%s' % (doc1 + ':' + doc2, similarity)
