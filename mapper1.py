#!/bin/python2

import sys


def clean_line(line):
    #TODO Remove spaces, |, punctuation, non ASCII characters, etc.
    ret_val = line.strip()
    return ret_val

def process_sentence(line):
    #Create a blank dictionary
    #Maps from word to count
    key_dict = dict()

    #Sentence format - Sentence number|Sentence
    cleaned_line = clean_line(line)
    
    split_line = cleaned_line.split('\t')

    sentence_number = split_line[0]
    try:
        sentence = split_line[1]
    except:
        sys.stderr.write ("Failed to split sentence %s" % (sentence_number))
        return

    split_sentence = sentence.split()

    for word in split_sentence:
        if(key_dict.has_key(word)):
            #Simply increment count
            key_dict[word] = key_dict[word] + 1
        else:
            #Start a fresh count
            key_dict[word] = 1

    square_sum = 0
    for key in key_dict.keys():
        square_sum += key_dict[key] * key_dict[key]

    #Now echo <Word, <Document number/doc length, count>>
    
    for word in key_dict.keys():
        key = word
        #value = str(sentence_number) + "/" + str(len(split_sentence))+ ":" + str(key_dict[key])
        value = str(sentence_number) + "/" + str(square_sum)+ ":" + str(key_dict[key])
        print '%s\t%s' % (key, value)



for line in sys.stdin:
    process_sentence(line)

