#!/bin/python2

import sys

current_word = None
count_dict = dict()

for line in sys.stdin:

    #Each input is of the form 'Word\t<DocID>:Count'
    split_line = line.split('\t')
    word = split_line[0]
    try:
        value = split_line[1]
    except:
        print "Mapper2: Problem with %s" % (word)
        continue
    split_value = value.split(':')
    sentence_number = split_value[0]
    count = split_value[1]

    if current_word != word:
        #Update current word
        current_word = word
        #Similarity dictionary mapping doesn't need to be updated       
        #Reset dictionary with fresh count
        count_dict = dict() 
        count_dict[sentence_number] = count
    else:
        count_dict[sentence_number] = count
        for first_sentence in count_dict.keys():
            if(first_sentence < sentence_number):
                #Echo a <Sentence1,Sentence2>,<Weight product> K-V pair
                key = first_sentence + '_' + sentence_number
                value = int(count_dict[first_sentence]) * int(count_dict[sentence_number])
                print '%s\t%s' % (key, value)
