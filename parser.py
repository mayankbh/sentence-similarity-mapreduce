import json


tweets_data_path = 'twitter_data.txt'
processed_tweets_path = 'twitter_sentences.txt'


tweets_data = []
tweets_file = open(tweets_data_path, "r")
sentences_file = open(processed_tweets_path, "w")

for line in tweets_file:
    try:
        tweet = json.loads(line)
        tweets_data.append(tweet)
    except:
        continue

print "Number of tweets is: " + str(len(tweets_data))
 

index = 1

for tweet in tweets_data:
    try:
        if tweet[u'lang'] == u'en':    #Only process English tweets
            tweet_text = tweet['text'].encode('utf-8')
            print str(index) + ":" + tweet_text
            sentences_file.write(str(index) + "\t" + tweet_text)
            print "Wrote " + str(index)
            index += 1
    except:
        continue
